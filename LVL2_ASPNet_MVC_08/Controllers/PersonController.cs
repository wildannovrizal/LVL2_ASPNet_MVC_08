﻿using LVL2_ASPNet_MVC_08.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_08.Controllers
{
    public class PersonController : Controller
    {
        db_targetEntities db = new db_targetEntities();
        // GET: Person
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetData(tbl_person person)
        {
            person = db.tbl_person.Where(x => x.PK_Person_ID == 1).FirstOrDefault();
            return Json(person, JsonRequestBehavior.AllowGet);
        } // kalau join join tinggal buat dulu view table di sql server, tarik ke asp .net, baru pake linq
    }
}